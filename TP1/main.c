//
//  main.c
//  TP1
//
//  Created by gaetan scarna on 28/09/2021.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void exercice1(void);
void exercice2(void);
void exercice3(void);
void exercice4(void);
void exercice5(void);
void exercice6(void);

int main(int argc, const char * argv[]) {
//    exercice1();
//    exercice2();
//    exercice3();
//    exercice4();
//    exercice5();
    exercice6();
    
    return 0;
}

void exercice1(void){
    
//    printf("%d\n%1d\n%4d\n", 17, 17, 17);
//    printf("%04d %016f %12s\n",17, 3.141593, "string");
//    printf("%5.3f\n%11.6f\n", 3.1415, 3.1415);
//    printf("%5.0f\n",314.0);
    printf(" à droite :\t %6d \n à gauche :\t*%-6d*\n", 314, 314);
}


void exercice2(void){
    float valeur;
    char devise[20];
    int i=0;
    printf("valeur a convertir svp : \n");
    scanf("%f", &valeur);
    fflush(stdin);
    while (i==0) {
        printf("\nDevise svp : \n");
        fgets(devise, 20, stdin);
        if(strcmp(devise, "Euros\n")==0 || strcmp(devise, "Dollars\n")==0){
            i=1;
        }
        
    }
    
    if(strcmp(devise, "Dollars")==0){
        valeur*=1.17;
    }else{
        valeur/=1.17;
    }
    
    printf("\n Vous avez %4.2f %s\n", valeur, devise);
    
    
}

void exercice3(void){
    
    int A[30];
    A[0]=1;
    A[1]=2;
    int nb=2;
    int B[10]={0,1,2,30,2,1,0,2,3};
    
    for (int i=nb; i<10; i++) {
        A[i]=B[i-nb];
    }
    
    for (int i=0; i<10; i++) {
        printf("%d\n", A[i]);
    }
    
}

void exercice4(void){
    char A[4][4]={{'a','b','c','d'},{'e','f','g','h'},{'i','j','k','l'},{'m','n','o','p'}};
    char B[16];
    int y=0;
    int autre=0;
    
    for (int i=0; i<4; i++) {
        do{
            B[y]=A[i][autre];
            autre++;
            y++;
        }while((y%4)!=0);
        autre=0;
    }
    
    for (int i=0; i<16; i++) {
        printf("%c ", B[i]);
    }
}


void exercice5(void){
    int tailleX;
    int tailleY;
    int valeur;
    int somme=0;
    float moyenne;
    printf("taille X svp\n");
    scanf("%d", &tailleX);
    printf("\ntaille Y svp\n");
    scanf("%d", &tailleY);
    int tab[tailleX][tailleY];
    for(int x = 0; x<tailleX;x++){
        for (int y=0; y<tailleY; y++) {
            printf("\n Une valeur svp \n");
            scanf("%d", &valeur);
            tab[x][y]=valeur;
            
        }
    }
    
    printf("\n");
    for(int x = 0; x<tailleX;x++){
        for (int y=0; y<tailleY; y++) {
            printf("%4d", tab[x][y]);
            somme=somme+tab[x][y];
            
        }
        printf("\n");
    }
    moyenne=(float)somme/(tailleY*tailleX);
    printf("\nLa somme est : %d et la moyenne est : %f\n", somme,moyenne);
    
}

void exercice6(void){
    char chaine[100];
    int i=0;
    char caractere[1];
    printf("Une phrase s'il vous plait\n");
    fgets(chaine, 100, stdin);
    printf("\nCaractere a supp\n");
    scanf("%c", &caractere[0]);
    while (caractere[0]!=chaine[i]) {
        i++;
    }
    chaine[i]=' ';
    i=0;
    while (chaine[i]!='\n') {
        printf("%c", chaine[i]);
        i++;
    }
    printf("\n");
    
    
    
}
